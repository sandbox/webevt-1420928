
-- SUMMARY --
This module provides a Drag & Drop Upload widget for a File and an Image field.


-- FEATURES --
* Drag & Drop Upload widget for a File and an Image field.
* Drag & Drop Upload element (dragndrop_upload).
* Upload progress bar support.


-- INSTALLATION --
* Install this module.
* Enable "Drag & Drop Upload Widget for an Image field" module if you want a
widget for an Image field
* Create a File or an Image field and choose the widget 'Drag & Drop Upload'
for it
* You can configure the widget in the field edit form.


-- CONTACT --

Current maintainers:
* WebEvt - http://drupal.org/user/856734
