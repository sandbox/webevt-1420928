/**
 * @file
 * Contains a behavior-function to add a previewer callback for an image.
 */

(function ($) {
  Drupal.behaviors.dragndropUploadWidgetImage = {
    attach: function (context, settings) {
      $('.droppable.droppable-image').once('dnd-upload-widget-image', function () {

        var $droppable = $(this);
        var previewCallback = function (event, previewCnt, file) {
          var $preview = file.$preview = $('.droppable-preview-image', previewCnt).last();
          $preview.data('file', file);

          var $previewCnt = $(previewCnt);
          $previewCnt.append($preview.clone());

          $('img', $preview).attr('src', event.target.result);

          $('.preview-remove', $preview).click(function () {
            var file = $(this).closest('.droppable-preview-image').data('file');
            $droppable.DnD().removeFile(file);
          });

          $preview.fadeIn();
        };
        $droppable.data('previewers', previewCallback);
      });
    }
  }
})(jQuery);
