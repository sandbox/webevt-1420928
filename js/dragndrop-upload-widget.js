/**
 * @file
 * Contains a DnD class,custom DnD jQuery plugin and a behavior-function.
 *
 * Settings are provided via Drupal.settings.dragndrop_upload variable.
 */

(function ($) {
  Drupal.behaviors.dragndropUploadWidget = {
    attach: function (context, settings) {
      $.each(Drupal.settings.dragndrop_upload, function (index) {
        var fieldEl = $('#' + index);
        var $droppable = fieldEl.find('.form-item .droppable');

        $droppable.once('dnd-upload-widget', function () {
          // Initialize a droppable area.
          $(this).DnD(index);

          var settings = $droppable.DnD().getSettings();

          // Find the Upload and Remove buttons.
          var $uploadButton = $('#' + settings.upload_button);
          var $removeButton = $('#' + settings.remove_button);

          // This is needed to make Upload button not to appear during the
          // files upload.
          $uploadButton.hide();

          // Attach necessary events to the Upload and Remove buttons if
          // upload event is set to the 'manual' (so the user should click
          // Upload or Remove button for any action to be taken).
          if (settings.event == 'manual') {
            // Show remove button when file was dropped and preview
            // created.
            $droppable.bind('dnd:preview:added', function () {
              $removeButton.show();
              $uploadButton.show();
            });

            $droppable.bind('dnd:files:sent, dnd:files:empty', function () {
              $removeButton.hide();
              $uploadButton.hide();
            });

            $removeButton.unbind('mousedown').bind('mousedown', function () {
              $droppable.DnD().removeFiles();
            });

            $uploadButton.unbind('mousedown').bind('mousedown', function (event) {
              event.preventDefault();
              event.stopPropagation();

              $droppable.DnD().send();
              return false;
            });
          }

          $droppable.bind('dnd:files:received', function () {
            // Send files if upload event is set to 'auto'.
            if (settings.event == 'auto') {
              $droppable.DnD().send();
            }

            // Hide preview message if files number has reached the cardinality.
            if (settings.cardinality != -1 && settings.cardinality <= $droppable.DnD().getFilesList().length) {
              $('.droppable-message', $droppable).hide();
            }
          });

          $('.droppable-browse-button', $droppable).click(function (event) {
            event.preventDefault();

            $('.droppable-standard-upload-hidden input', fieldEl).click();

            return false;
          });

          // Attach the change event to the file input element to track and add
          // to the droppable area files added by the Browse button.
          $('.droppable-standard-upload-hidden input', fieldEl).unbind('change').change(function (event) {
            // Clone files array before clearing the input element.
            var transFiles = $.extend({}, event.target.files);
            // Clear the input element before adding files to the droppable area,
            // because if auto uploading is enabled, files are sent twice - from
            // the input element and the droppable area.
            $(this).val('');
            $droppable.DnD().addFiles(transFiles);
          });

          Drupal.behaviors.dragndropUploadWidget.attachValidators($droppable);
          Drupal.behaviors.dragndropUploadWidget.attachPreviewers($droppable);
        });
      });
    },

    attachValidators: function ($droppable) {
      var settings = $droppable.DnD().getSettings();
      var validators = [];

      if (settings.file_upload_max_size) {
        /**
         * File size validator.
         *
         * @param file
         * @returns {boolean}
         */
        var validateFileSize = function (file) {
          var errorMessage = Drupal.t("The selected file %filename cannot be uploaded, because it exceeds the maximum file size for uploading.", {
            // According to the specifications of HTML5, a file upload control
            // should not reveal the real local path to the file that a user
            // has selected. Some web browsers implement this restriction by
            // replacing the local path with "C:\fakepath\", which can cause
            // confusion by leaving the user thinking perhaps Drupal could not
            // find the file because it messed up the file path. To avoid this
            // confusion, therefore, we strip out the bogus fakepath string.
            '%filename': file.name.replace('C:\\fakepath\\', '')
          });
          return (file.size <= settings.file_upload_max_size) ? true : errorMessage;
        };

        validators.push(validateFileSize);
      }

      if (settings.file_validate_extensions) {
        /**
         * File extension validator.
         *
         * @param file
         * @returns {boolean|*}
         */
        var validateFileExt = function (file) {
          var errorMessage = Drupal.t("The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.", {
            '%filename': file.name.replace('C:\\fakepath\\', ''),
            '%extensions': settings.file_validate_extensions.join(', ')
          });
          var ext = file.name.split('.').pop();
          var isValid = false;

          $.each(settings.file_validate_extensions, function (index, allowedExt) {
            if (allowedExt == ext) {
              isValid = true;
              return false;
            }
          });

          return isValid || errorMessage;
        };

        validators.push(validateFileExt);
      }

      /**
       * Files number validator.
       *
       * @param file
       * @param filesList
       * @returns {boolean|*}
       */
      var validateFilesNum = function (file, filesList) {
        var errorMessage = Drupal.t('Too many files. Maximum allowed number of files is exceeded.');
        return (settings.cardinality == -1 || filesList.length < settings.cardinality) || errorMessage;
      };

      validators.push(validateFilesNum);

      // Add validators.
      $droppable.data('validators', validators);
    },

    attachPreviewers: function ($droppable) {
      // Define a preview callback for a file.
      var previewCallback = function (event, previewCnt, file) {
        var fileSize = file.file.size;
        var sizes = [Drupal.t('@size B'), Drupal.t('@size KB'), Drupal.t('@size MB'), Drupal.t('@size GB')];
        for (var i in sizes) {
          if (fileSize > 1024) {
            fileSize /= 1024;
          }
          else {
            fileSize = sizes[i].replace('@size', fileSize.toPrecision(2));
            break;
          }
        }

        var $preview = file.$preview = $('.droppable-preview-file', previewCnt).last();
        $preview.data('file', file);

        var $previewCnt = $(previewCnt);
        $previewCnt.append($preview.clone());

        $('.preview-filename', $preview).html(file.file.name);
        $('.preview-filesize', $preview).html(fileSize);

        $('.preview-remove', $preview).click(function () {
          var file = $(this).closest('.droppable-preview-file').data('file');
          $droppable.DnD().removeFile(file);
        });

        $preview.fadeIn();
      };
      $droppable.data('previewers', previewCallback);
    }
  }
})(jQuery);

/**
 * DnD magic.
 *
 * @param {jQuery} droppable
 *  jQuery object of droppable areas.
 *  Each area must have an validators function in it.
 *  Eg. $('.droppable').data('validators', [validateFunction1, validateFunction1]);
 *
 *  To see how to build validator function @see validateFile.
 */
function DnD(droppable) {
  this.$droppable = jQuery(droppable).eq(0);
  this.attachEvents();
}

(function ($) {
  // Get plain jQuery version and check whether is needed to apply data trick in
  // ajax options. See line 428 for comments.
  var jQueryVersion = parseInt($.fn.jquery.split('.').join(''));
  var applyDataTrick = jQueryVersion < 150;

  DnD.prototype = {
    $droppable: null,

    /**
     * Attach events to the given droppable areas.
     */
    attachEvents: function () {
      this.$droppable[0].ondrop = this.eventsList.drop.bind(this);
      this.$droppable[0].ondragover = this.eventsList.dragover.bind(this);
      this.$droppable[0].ondragleave = this.eventsList.dragleave.bind(this);
    },

    eventsList: {
      /**
       * Fires when file was dropped in the droppable area.
       *
       * @param event
       */
      drop: function (event) {
        // Prevent drop event from bubbling through parent elements.
        event.stopPropagation();
        event.preventDefault();

        var transFiles = event.dataTransfer.files;
        if (transFiles.length == 0) {
          return;
        }

        var $droppable = this.$droppable;

        $droppable.removeClass('drag-over').addClass('dropped');

        this.addFiles(transFiles);
      },

      /**
       * Fires every time when file is under the droppable area.
       *
       * @param event
       */
      dragover: function (event) {
        // Prevent the event from bubbling through parent elements.
        event.stopPropagation();
        event.preventDefault();

        this.$droppable.addClass('drag-over');
      },

      /**
       * Fires when file was leave the droppable area.
       *
       * @param event
       */
      dragleave: function (event) {
        // Prevent the event from bubbling through parent elements.
        event.stopPropagation();
        event.preventDefault();

        this.$droppable.removeClass('drag-over');
      }
    },

    /**
     * Add files to the droppable area.
     *
     * @param {Array} transFiles
     *  Array of files that should be added to the dropppable area.
     */
    addFiles: function (transFiles) {
      var file, error, messages = [], filesList = this.getFilesList();
      for (var i = 0, n = transFiles.length; i < n; i++) {
        error = this.validateFile(transFiles[i], filesList, this.$droppable.data('validators'));
        if (error !== true) {
          messages.push(error);
          continue;
        }

        file = {
          file: transFiles[i],
          $preview: null,
          errorStatus: !error,
          errorMessage: error
        };
        filesList.push(file);

        /**
         * Each file have:
         *  - file {Object}: dropped file object.
         *  - $preview {jQuery|null}: preview object.
         *  - errorStatus {Boolean}: whether file pass validation or not.
         *  - error {String}: error message if present.
         *
         * @type {Array}
         */
        this.setFilesList(filesList);

        this.$droppable.trigger('dnd:files:added', file);

        // Create a preview.
        this.createPreview(file);
      }

      if (messages.length) {
        this.showErrors(messages);
      }

      // Notify element that files already received.
      this.$droppable.trigger('dnd:files:received', transFiles);
    },

    /**
     * Get settings for the given droppable.
     *
     * @returns {*|boolean}
     */
    getSettings: function () {
      return Drupal.settings.dragndrop_upload[this.$droppable.data('field')] || false;
    },

    /**
     * Create previews of dropped files.
     *
     * @param file
     */
    createPreview: function (file) {
      var $previewCnt = $('.droppable-preview', this.$droppable).show();
      var reader = new FileReader();
      var me = this;

      reader.onload = function (event) {
        var previewersList = [];
        var previewers = me.$droppable.data('previewers') || [];

        if (typeof previewers == 'function') {
          previewersList.push(previewers);
        } else {
          previewersList = previewers;
        }

        // Iterate through all previewers functions.
        for (var i = 0, n = previewersList.length; i < n; i++) {
          previewersList[i](event, $previewCnt, file);
        }

        // Trigger event telling a preview has been created for a file.
        me.$droppable.trigger('dnd:preview:added', [event, $previewCnt, file]);
      };
      reader.readAsDataURL(file.file);
    },

    /**
     * Remove preview from droppable area.
     *
     * @param file
     */
    removePreview: function (file) {
      var $previewCnt = $('.droppable-preview', this.$droppable);

      if (file) {
        file.$preview.remove();
        // Trigger event telling that a preview has been removed.
        this.$droppable.trigger('dnd:preview:removed', [$previewCnt, file]);
      }
      else {
        $('>:not(:last-child)', $previewCnt).remove();
      }

      if ($previewCnt.children().size() == 1) {
        $previewCnt.hide();
        this.$droppable.removeClass('dropped');
        $('.droppable-message', this.$droppable).show();

        // Trigger event telling that there are no any previews remain.
        this.$droppable.trigger('dnd:preview:empty', [$previewCnt, file]);
      }
    },

    /**
     * Validate file by given function.
     *
     * @param file
     * @param filesList
     *  Array of files already dropped.
     * @param {Function} validators
     *  Example of validator function.
     *
     *  var validateFunction = function(file) {
         *    var errorMessage = 'Max file size exceed'.
         *    var maxSize = 10000;
         *
         *    return (file.size <= maxSize) ? true : errorMessage;
         *  }
     *
     * @returns {String|Boolean}
     *  Return error message if not valid or True when valid.
     */
    validateFile: function (file, filesList, validators) {
      var validatorsList = [];
      var errorMessage = true;
      validators = validators || [];

      if (typeof validators == 'function') {
        validatorsList.push(validators);
      } else {
        validatorsList = validators;
      }

      // Iterate through all validators functions.
      for (var i = 0, n = validatorsList.length; i < n; i++) {
        errorMessage = validatorsList[i](file, filesList);
        if (errorMessage !== true) {
          return errorMessage;
        }
      }

      return errorMessage;
    },

    /**
     * Show errors for the droppable.
     *
     * @param messages
     */
    showErrors: function (messages) {
      if (typeof messages != 'object') {
        messages = [messages];
      }

      messages = messages.join('<br/>');

      // Remove any previous errors.
      $('.file-upload-js-error').remove();
      this.$droppable.closest('div.form-managed-file').prepend('<div class="messages error file-upload-js-error">' + messages + '</div>');
    },

    /**
     * Remove a file from droppable area.
     *
     * @param file
     *  The file that should be removed.
     */
    removeFile: function (file) {
      var me = this;
      var droppedFiles = me.getFilesList();

      $.each(droppedFiles, function (index, eachFile) {
        if (file == eachFile) {
          droppedFiles.splice(index, 1);
          me.removePreview(file);
        }
      });

      me.setFilesList(droppedFiles);

      // Trigger necessary events.
      this.$droppable.trigger('dnd:files:removed', file);
      if (!droppedFiles.length) {
        this.$droppable.trigger('dnd:files:empty');
      }
    },

    /**
     * Remove files from the droppable area.
     *
     * @param files
     *  Files to be removed. Removes all files if undefined.
     */
    removeFiles: function (files) {
      var me = this;
      if (files) {
        $.each(files, function (index, eachFile) {
          me.removeFile(eachFile);
        });
      }
      else {
        me.removePreview();
        me.setFilesList([]);
      }

      this.$droppable.trigger('dnd:files:empty');
    },

    /**
     * Get files list of the droppable area.
     *
     * @returns {*|Array}
     */
    getFilesList: function () {
      return this.$droppable.data('files') || [];
    },

    /**
     * Get files list of the droppable area.
     *
     * @param filesList
     */
    setFilesList: function (filesList) {
      filesList = filesList || [];
      this.$droppable.data('files', filesList);
    },

    /**
     * Send files.
     */
    send: function () {
      var $droppable = this.$droppable;
      var filesList = this.getFilesList();
      if (!filesList.length) {
        return;
      }

      var formEl = $droppable.closest('form');
      var form = new FormData(formEl.get(0));
      var settings = this.getSettings();

      // Append filesList to the form.
      $.each(filesList, function (index, file) {
        form.append(settings.name, file.file);
      });

      var $uploadButton = $('#' + settings.upload_button);
      // These parameters are needed for Drupal to determine that ajax
      // request has been initiated by Upload button.
      form.append('_triggering_element_name', $uploadButton.attr('name'));
      form.append('_triggering_element_value', $uploadButton.attr('value'));

      /**
       * Send form data and process the response.
       */
      var ajax = Drupal.ajax[settings.upload_button];
      var options = {
        url: ajax.url,
        // Trick to overcome jQuery Update dependency.
        // Do not set data here because of incorrent handling of content-type
        // header in jQuery 1.4.4. Instead, set it in the beforeSend callback.
        // data: form,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        beforeSend: function (xmlhttprequest, options) {
          // Add ajax_html_ids and ajax_page_state info
          // @see Drupal.ajax.prototype.beforeSerialize()

          // Allow detaching behaviors to update field values before collecting them.
          // This is only needed when field values are added to the POST data, so only
          // when there is a form such that this.form.ajaxSubmit() is used instead of
          // $.ajax(). When there is no form and $.ajax() is used, beforeSerialize()
          // isn't called, but don't rely on that: explicitly check this.form.
          if (ajax.form) {
            var settings = ajax.settings || Drupal.settings;
            Drupal.detachBehaviors(ajax.form, settings, 'serialize');
          }

          // Request data must be set here for jQuery version < 1.5.0.
          // See line 428 for the comments.
          if (applyDataTrick) {
            options.data = form;
          }

          // Prevent duplicate HTML ids in the returned markup.
          // @see drupal_html_id()
          var ajaxHtmlIds = $.map($('[id]'), function (element, index) {
            return element.id;
          });

          // Add Drupal-specific data to the request.
          options.data.append('ajax_html_ids[]', ajaxHtmlIds.join(','));

          // Allow Drupal to return new JavaScript and CSS files to load without
          // returning the ones already loaded.
          // @see ajax_base_page_theme()
          // @see drupal_get_css()
          // @see drupal_get_js()
          options.data.append('ajax_page_state[theme]', Drupal.settings.ajaxPageState.theme);
          options.data.append('ajax_page_state[theme_token]', Drupal.settings.ajaxPageState.theme_token);
          for (var key in Drupal.settings.ajaxPageState.css) {
            options.data.append('ajax_page_state[css][' + key + ']', 1);
          }
          for (key in Drupal.settings.ajaxPageState.js) {
            options.data.append('ajax_page_state[js][' + key + ']', 1);
          }

          ajax.ajaxing = true;

          return ajax.beforeSend(xmlhttprequest, options);
        },
        // Process the response with Drupal native way:        // execute received commands like when the ajax request
        // has been initiated by Upload button.
        success: function (response, status) {
          $droppable.trigger('dnd:files:sent');
          return ajax.options.success(response, status);
        },
        complete: ajax.options.complete
      };
      // Set data for the request, if was not set in the beforeSend
      // callback (jQuery version is 1.5.0 or higher).
      if (!applyDataTrick) {
        options.data = form;
      }
      // Finally, send a request.
      $.ajax(options);
    }
  };

  /**
   * jQuery plugin to help to work with DnD class.
   *
   * @param arg
   *  Droppable element to initialize or a command
   *
   * @returns {*}
   * @constructor
   */
  $.fn.DnD = function (arg) {
    var dnd = this.data('dnd');
    if (!dnd) {
      this.data('dnd', new DnD(this));
      this.data('field', arg)
    }

    return dnd;
  };
})(jQuery);
